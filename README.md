## dublin-bus ##

Sample exercise to process data from dublin bus operators and vehicles.


## Requirements:

- docker and docker-compose
- Java 8  or above
- SBT


## Stack:

- Scala
- SBT as build tool
- Http4S framework as Web library
- Doobie for relational database access
- PostgreSQL as backend Database
- Cats and Cats-effect as functional programming supporting libraries
- Refined for more type safety
- Circe for json support
- Kantan.csv for CSV support
-  Specs2 for testing
- 




## Database Management system choice:

I chose PostreSQL 11 to implement the data layer of the application. It's not as fast or specific as some NoSQL solutions, but it's performant, predictable and I'm familiar with it.

Cassandra, for instance, could be a better fit, namely on read performance, given data is properly partitioned and duplicated (if needed).
I was not able to put that to work properly, mainly due to the existing cassandra scala libraries not integrating well with the rest of the stack, and probably the long time I don't work with Cassandra in a daily context.

Creating suitable indexes on the data made query time and plan suitable to handle at least 100 requests per second in my local machine, running PostgreSQL and docker in a VM (as per the usual Mac OS docker setup).

Database schema can be found at src/resources/db/migrations

It's completely managed by the application using Flyway, so no need for any loading prior schema creation.

## Web Toolkit:

The choice for Http4S is mostly based on preference: either Akka-http, Play framework, or other would work regarding the specified requirements.

The fact that it is purely functional, asynchronous and supports streaming directly from the database (see how vehicle trace is implemented) made the implementation streight-forward (given one is familiar with all the libraries and edioms).



## Running the application

Make sure PostgreSQL is running before anything:

```sh
docker-compose up -d
```


### Loading data

Data Loader script is provided by the application itself (probably not the best solution for production but usable on this case)

Just use the provided sbt alias:

```sh
sbt "runLoader example.csv"
```

Where example.csv is the file name containing the CSV data for dublin bus.

Tested with the file from 2013-01-01.

## Running the server:

Again use SBT alias:

```sh
sbt runServer
```

Look at the logs to make sure it started properly, and listening on port 8000.


## Example requests:



### 1. Running operators:

```sh
curl "http://localhost:8000/operators?start-time=2013-01-01t09:00&end-time=2013-01-01T10:00:00"

```

### 2. Vehicle ids by operator:


```sh
curl "http://localhost:8000/operators/CD/vehicles?start-time=2013-01-01t09:00&end-time=2013-01-01T10:00:00"
```




### 3. Vehicle ids by operator at stop:


```sh
curl "http://localhost:8000/operators/CD/stoped-vehicles?start-time=2013-01-01t09:00&end-time=2013-01-01T10:00:00"
```



### 4. Vehicle trace:

```sh
curl "http://localhost:8000/vehicles/33594/trace?start-time=2013-01-01t09:00&end-time=2013-01-01T10:00:00"
```


## Running Tests

Just do

```sh
sbt test
```

At the moment only basic unit tests for the HTTP API and CSV loader are implemented.
Integration tests are surely needed, didn't implement due to time constraints.

