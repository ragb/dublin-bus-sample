import sbt._

object Dependencies {

  lazy val catsEffect = "org.typelevel" %% "cats-effect" % catsEffectVersion
  lazy val doobieDependencies = Seq(
    "org.tpolecat" %% "doobie-core",
    "org.tpolecat" %% "doobie-hikari",
    "org.tpolecat" %% "doobie-postgres",
    "org.tpolecat" %% "doobie-refined"
  ).map(_ % doobieVersion)

  lazy val postgresqlDriver = "org.postgresql" % "postgresql" % postgresqlDriverVersion
  lazy val flywayCore = "org.flywaydb" % "flyway-core" % flywayVersion

  lazy val circeCore = "io.circe" %% "circe-core" % circeVersion
  lazy val circeGeneric = "io.circe" %% "circe-generic" % circeVersion
  lazy val circeRefined = "io.circe" %% "circe-refined" % circeVersion

  lazy val http4sBlazeServer = "org.http4s" %% "http4s-blaze-server" % http4sVersion
  lazy val http4sCirce = "org.http4s" %% "http4s-circe" % http4sVersion
  lazy val http4sDsl = "org.http4s" %% "http4s-dsl" % http4sVersion
  lazy val logback = "ch.qos.logback" % "logback-classic" % logbackVersion
  lazy val scalaLogging = "com.typesafe.scala-logging" %% "scala-logging" % scalaLoggingVersion

  lazy val cirisDependencies = Seq(
    "is.cir" %% "ciris-core",
    "is.cir" %% "ciris-cats",
    "is.cir" %% "ciris-refined",
    "is.cir" %% "ciris-cats-effect"
  ).map(_ % cirisVersion)

  lazy val refined = "eu.timepit" %% "refined" % refinedVersion
  lazy val refinedCats = "eu.timepit" %% "refined-cats" % refinedVersion

  lazy val kantanCsvDependencies = Seq("com.nrinaudo" %% "kantan.csv",
                                       "com.nrinaudo" %% "kantan.csv-generic",
                                       "com.nrinaudo" %% "kantan.csv-java8",
                                       "com.nrinaudo" %% "kantan.csv-refined")
    .map(_ % kantanCsvVersion)

  lazy val specs2Core = "org.specs2" %% "specs2-core" % specs2Version
  lazy val specs2Scalacheck = "org.specs2" %% "specs2-scalacheck" % specs2Version
  lazy val dockerTestKitDependencies = Seq(
    "com.whisk" %% "docker-testkit-specs2",
    "com.whisk" %% "docker-testkit-impl-spotify"
  ).map(_ % dockerTestKitVersion)
  lazy val scalacheck = "org.scalacheck" %% "scalacheck" % scalacheckVersion

  val specs2Version = "4.7.0"
  val scalacheckVersion = "1.14.0"
  val kindProjectorVersion = "0.10.3"

  val catsEffectVersion = "1.4.0"
  val circeVersion = "0.11.1"
  val http4sVersion = "0.20.10"
  val logbackVersion = "1.2.3"
  val cirisVersion = "0.12.1"
  val refinedVersion = "0.9.9"
  val doobieVersion = "0.7.0"
  val postgresqlDriverVersion = "42.2.6"
  val kantanCsvVersion = "0.5.1"
  val flywayVersion = "5.2.4"
  val scalaLoggingVersion = "3.9.2"
  val dockerTestKitVersion = "0.9.9"
}
