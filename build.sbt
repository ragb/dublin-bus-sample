import Dependencies._

lazy val root = (project in file("."))
  .configs(IntegrationTest)
  .settings(
    inThisBuild(
      List(
        organization := "com.ruiandrebatista",
        scalaVersion := "2.12.8",
        version := "0.1.0-SNAPSHOT",
        organizationName := "Rui Batista",
        startYear := Some(2019),
        licenses += ("Apache-2.0", new URL(
          "https://www.apache.org/licenses/LICENSE-2.0.txt"))
      )),
    Defaults.itSettings,
    name := "dublin-bus",
    libraryDependencies ++= Seq(
      http4sBlazeServer,
      http4sDsl,
      http4sCirce,
      circeGeneric,
      circeCore,
      circeRefined,
      catsEffect,
      refined,
      refinedCats,
      logback,
      scalaLogging,
      postgresqlDriver,
      flywayCore,
      specs2Core % Test,
      specs2Scalacheck % Test,
      scalacheck % Test
    ) ++ cirisDependencies ++ kantanCsvDependencies ++ doobieDependencies ++ dockerTestKitDependencies
      .map(_ % "it"),
    addCompilerPlugin(
      "org.typelevel" % "kind-projector" % kindProjectorVersion cross CrossVersion.binary),
    git.useGitDescribe := true
  )
  .enablePlugins(GitVersioning)

addCommandAlias("runServer", "runMain com.ruiandrebatista.bus.Server")
addCommandAlias("runLoader", "runMain com.ruiandrebatista.bus.CsvLoader")
