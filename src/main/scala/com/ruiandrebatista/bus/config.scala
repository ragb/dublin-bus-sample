package com.ruiandrebatista.bus

import cats.effect.IO
import ciris._
import ciris.cats.effect._
import ciris.refined._

import eu.timepit.refined.types.net.UserPortNumber
import eu.timepit.refined.string.IPv4

import eu.timepit.refined.api._
import eu.timepit.refined.auto._
import eu.timepit.refined.types.string.NonEmptyString
import eu.timepit.refined.types.numeric.PosInt

final case class Config(
    http: HttpConfig,
    database: DatabaseConfig
)

final case class HttpConfig(
    interface: String Refined IPv4,
    port: UserPortNumber
)

final case class DatabaseConfig(
    user: NonEmptyString,
    password: String,
    jdbcUrl: NonEmptyString,
    driverClassName: String,
    maxPoolSize: PosInt
)

object Config {
  def load =
    loadConfig(
      propF[IO, Option[String Refined IPv4]]("http.interface"),
      propF[IO, Option[UserPortNumber]]("http.port"),
      envF[IO, Option[NonEmptyString]]("DATABASE_URL")
    ) { (interface, port, databaseUrl) =>
      Config(
        HttpConfig(
          interface = interface.getOrElse("0.0.0.0"),
          port = port.getOrElse(8000)
        ),
        DatabaseConfig(
          jdbcUrl =
            databaseUrl.getOrElse("jdbc:postgresql://localhost/postgres"),
          user = "postgres",
          password = "",
          maxPoolSize = 64,
          driverClassName = "org.postgresql.Driver"
        )
      )

    }.orRaiseThrowable

}
