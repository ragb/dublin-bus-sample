package com.ruiandrebatista.bus

import cats.effect._

import org.http4s._
import org.http4s.implicits._
import org.http4s.dsl.io._

import com.ruiandrebatista.bus.data.BusQueryRepository
import java.time.Instant
import java.time.format.DateTimeFormatter
import io.circe.refined._
import io.circe.syntax._
import org.http4s.circe._
import eu.timepit.refined.types.string.NonEmptyString
import eu.timepit.refined.auto._

import org.http4s.server.blaze.BlazeServerBuilder
import org.http4s.server.middleware.Logger
import java.time.ZoneId

import fs2._

class HttpApi(queryRepository: BusQueryRepository[IO]) {

  // Machinery to decode instant values (timestamps) as query parameters

  // format for timestamps
  // We assume system default timezone for those (should be explicit in most cases)
  implicit val instantCodec =
    QueryParamCodec.instantQueryParamCodec(
      DateTimeFormatter.ISO_LOCAL_DATE_TIME.withZone(ZoneId.systemDefault())
    )

  object startTimeQueryMatcher
      extends QueryParamDecoderMatcher[Instant]("start-time")

  object endTimeQueryMatcher
      extends QueryParamDecoderMatcher[Instant]("end-time")

  // Match nonEmpty strings (probably this exists on a library somewhere)
  object nonEmptyStringVar {
    def unapply(str: String): Option[NonEmptyString] =
      NonEmptyString.from(str).toOption
  }

  def routes = HttpRoutes.of[IO] {
    case GET -> Root / "operators" :? startTimeQueryMatcher(start) +& endTimeQueryMatcher(
          end
        ) =>
      Ok(queryRepository.listRunningOperators(start, end).map(_.asJson))

    case GET -> Root / "operators" / nonEmptyStringVar(operator) / "vehicles" :? startTimeQueryMatcher(
          start
        ) +& endTimeQueryMatcher(end) =>
      Ok(
        queryRepository
          .listRunningVehicleIds(operator, start, end)
          .map(_.asJson)
      )

    case GET -> Root / "operators" / nonEmptyStringVar(operator) / "stoped-vehicles" :? startTimeQueryMatcher(
          start
        ) +& endTimeQueryMatcher(end) =>
      Ok(
        queryRepository
          .listRunningVehicleIdsAtStop(operator, start, end)
          .map(_.asJson)
      )

    case GET -> Root / "vehicles" / nonEmptyStringVar(vehicleId) / "trace" :? startTimeQueryMatcher(
          start
        ) +& endTimeQueryMatcher(end) =>
      // Take care to wrap the stream in a Json array
      Ok(
        Stream.emit("[") ++
          queryRepository
            .traceVehicle(vehicleId, start, end)
            .map(_.asJson.noSpaces)
            .intersperse(",") ++
          Stream.emit("]")
      )

  }

}

object HttpServer {

  def server(
      config: HttpConfig,
      routes: HttpRoutes[IO]
  )(implicit cs: ContextShift[IO], timer: Timer[IO]) =
    BlazeServerBuilder[IO]
      .bindHttp(config.port, config.interface)
      .withHttpApp(Logger.httpApp(false, false)(routes.orNotFound))
      .resource

}
