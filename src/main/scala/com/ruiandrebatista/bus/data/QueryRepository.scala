package com.ruiandrebatista.bus.data

import cats.effect.IO
import java.time.Instant
import doobie._
import doobie.implicits._
import doobie.refined.implicits._

import eu.timepit.refined.types.string.NonEmptyString
import eu.timepit.refined.types.string

import io.circe.Encoder
import io.circe.generic.semiauto._
import io.circe.refined._
import fs2._

final case class TraceEntry(
    timestamp: Instant,
    coordinates: (BigDecimal, BigDecimal),
    stopedAt: Option[NonEmptyString]
)

object TraceEntry {
  implicit val encoder: Encoder[TraceEntry] = deriveEncoder
}
trait BusQueryRepository[F[_]] {
  def listRunningOperators(start: Instant,
                           end: Instant): F[Vector[NonEmptyString]]
  def listRunningVehicleIds(operator: NonEmptyString,
                            start: Instant,
                            end: Instant): F[Vector[NonEmptyString]]
  def listRunningVehicleIdsAtStop(operator: NonEmptyString,
                                  start: Instant,
                                  end: Instant): F[Vector[NonEmptyString]]
  def traceVehicle(vehicleId: NonEmptyString,
                   start: Instant,
                   end: Instant): Stream[F, TraceEntry]
}

class PostgresBusQueryRepository(transactor: Transactor[IO])
    extends BusQueryRepository[IO] {

  def listRunningOperators(start: Instant,
                           end: Instant): IO[Vector[string.NonEmptyString]] =
    sql"""
        SELECT DISTINCT operator FROM bus_data WHERE "timestamp" BETWEEN $start AND $end ORDER BY operator
      """
      .query[NonEmptyString]
      .to[Vector]
      .transact(transactor)

  def listRunningVehicleIds(operator: string.NonEmptyString,
                            start: Instant,
                            end: Instant): IO[Vector[string.NonEmptyString]] =
    sql"""
         SELECT DISTINCT vehicle_id FROM bus_data WHERE operator = $operator AND "timestamp" BETWEEN $start AND $end ORDER BY vehicle_id
      """
      .query[NonEmptyString]
      .to[Vector]
      .transact(transactor)

  def listRunningVehicleIdsAtStop(
      operator: string.NonEmptyString,
      start: Instant,
      end: Instant): IO[Vector[string.NonEmptyString]] =
    sql"""
         SELECT DISTINCT vehicle_id FROM bus_data WHERE operator = $operator AND "timestamp" BETWEEN $start AND $end AND at_stop = true ORDER BY vehicle_id
      """
      .query[NonEmptyString]
      .to[Vector]
      .transact(transactor)

  def traceVehicle(vehicleId: string.NonEmptyString,
                   start: Instant,
                   end: Instant): Stream[IO, TraceEntry] =
    sql"""
        SELECT "timestamp", coordinates[0], coordinates[1],
        CASE WHEN at_stop THEN stop_id ELSE NULL END
           FROM bus_data WHERE vehicle_id = $vehicleId
                AND "timestamp" BETWEEN $start AND $end order by "timestamp" asc
      """
      .query[TraceEntry]
      .stream
      .transact(transactor)
}
