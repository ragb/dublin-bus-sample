package com.ruiandrebatista.bus.data

import cats.instances.list._
import cats.instances.int._
import doobie._
import doobie.implicits._

import doobie.refined.implicits._

import com.ruiandrebatista.bus.model._

import cats.effect.IO

import fs2._

import doobie.util.transactor.Transactor
import eu.timepit.refined.types.string.NonEmptyString
import eu.timepit.refined.auto._
import java.time.Instant
import com.typesafe.scalalogging.LazyLogging
import cats.effect.ContextShift

final case class LoadDataResult(rowsLoaded: Int)

trait BusLoadRepository[F[_]] {
  def loadData(data: Stream[F, RawCsvRow]): F[LoadDataResult]
}

class PostgresBusLoadRepository(transactor: Transactor[IO])(
    implicit cs: ContextShift[IO]
) extends BusLoadRepository[IO]
    with LazyLogging {

  def loadData(data: Stream[IO, RawCsvRow]): IO[LoadDataResult] =
    data
      .chunkN(32768)
      .evalMap { chunk =>
        val sql =
          """
          INSERT INTO "bus_data" ("operator", "timestamp", "vehicle_id", "coordinates", "stop_id", "at_stop") VALUES
            (?, ?, ?, POINT(?, ?), ?, ?)
            ON CONFLICT DO NOTHING;
      """

        type BusDataRow = (
            NonEmptyString,
            Instant,
            NonEmptyString,
            BigDecimal,
            BigDecimal,
            NonEmptyString,
            Boolean
        )

        val rows = chunk.map { raw =>
          (
            raw.operator,
            Instant.ofEpochSecond(
              raw.timestampMicros / 1000000,
              raw.timestampMicros % 1000000
            ),
            raw.vehicleId,
            raw.lon,
            raw.lat,
            raw.stopId,
            raw.atStop
          )
        }

        Update[BusDataRow](sql)
          .updateMany(rows.toList)
          .transact(transactor)

      }
      .observe { numbers =>
        numbers.evalMap(n => IO(logger.info(s"Loaded $n rows")))
      }
      .foldMonoid
      .map(LoadDataResult.apply _)
      .compile
      .lastOrError
}
