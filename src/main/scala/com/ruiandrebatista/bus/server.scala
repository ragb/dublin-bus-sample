package com.ruiandrebatista.bus

import cats.effect.IO

import com.ruiandrebatista.bus.data.PostgresBusQueryRepository

object Server extends AbstractApp {
  def runFromConfig(args: List[String], config: Config) =
    (for {
      appResources <- resources(config)
      queryRepository = new PostgresBusQueryRepository(appResources.transactor)
      httpApi = new HttpApi(queryRepository)
      _ <- HttpServer.server(config.http, httpApi.routes)
    } yield ()).use(_ => IO.never)

}
