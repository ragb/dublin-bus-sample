package com.ruiandrebatista.bus

import cats.implicits._
import cats.effect._

import doobie.util.ExecutionContexts
import _root_.scala.concurrent.ExecutionContext
import doobie.hikari.HikariTransactor

case class AppResources(
    blockingEc: ExecutionContext,
    transactor: HikariTransactor[IO]
)

trait AbstractApp extends IOApp {

  def run(args: List[String]) = Config.load >>= (c => runFromConfig(args, c))
  def runFromConfig(args: List[String], config: Config): IO[ExitCode]

  protected def resources(config: Config) =
    for {
      blockingEc <- ExecutionContexts.cachedThreadPool[IO]
      transactor <- Database.connectionPool(config.database)(blockingEc)
      _ <- Resource.liftF(Migrations.migrate(transactor))
    } yield AppResources(blockingEc, transactor)
}
