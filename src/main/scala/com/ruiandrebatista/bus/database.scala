package com.ruiandrebatista.bus

import cats.effect.{ContextShift, IO, Resource}

import com.typesafe.scalalogging.LazyLogging
import doobie._
import doobie.hikari._

import eu.timepit.refined.auto._
import org.postgresql.ds.PGSimpleDataSource

import scala.concurrent.ExecutionContext

object Database extends LazyLogging {

  def connectionPool(config: DatabaseConfig)(
      blockingExecutionContext: ExecutionContext)(
      implicit sf: ContextShift[IO]): Resource[IO, HikariTransactor[IO]] = {
    import config._
    for {
      cs <- ExecutionContexts.fixedThreadPool[IO](config.maxPoolSize) // Connection aquire thread pull
      xa <- HikariTransactor.newHikariTransactor[IO](driverClassName,
                                                     jdbcUrl,
                                                     user,
                                                     password,
                                                     cs,
                                                     blockingExecutionContext)
      _ <- Resource.liftF(xa.configure { config =>
        IO {

          config.setMaximumPoolSize(maxPoolSize)

        }
      })
      _ = logger.info(s"Started connection pool. Maximum size $maxPoolSize")
    } yield xa

  }

  def dataSource(config: DatabaseConfig)(
      blockingExecutionContext: ExecutionContext)(
      implicit sc: ContextShift[IO]) =
    for {
      cs <- ExecutionContexts.fixedThreadPool[IO](config.maxPoolSize) // Connection aquire thread pull
      ds <- Resource.liftF(IO {
        val ds = new PGSimpleDataSource
        ds.setURL(config.jdbcUrl)
        ds.setUser(config.user)
        ds.setPassword(config.password)
        ds
      })
      xa = Transactor.fromDataSource[IO][PGSimpleDataSource](
        ds,
        cs,
        blockingExecutionContext)
    } yield xa

}
