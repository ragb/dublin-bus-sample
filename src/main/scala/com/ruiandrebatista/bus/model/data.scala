package com.ruiandrebatista.bus.model

import java.time.Instant

final case class OperatorData(
    operator: String,
    timestamp: Instant,
    vehicleId: String,
    stopId: String,
    atStop: Boolean
)
