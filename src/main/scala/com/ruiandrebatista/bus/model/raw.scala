package com.ruiandrebatista.bus.model

import eu.timepit.refined.types.all._

import java.time.LocalDate

final case class RawCsvRow(
    timestampMicros: PosLong,
    lineId: Option[PosInt],
    direction: Boolean, // ignored
    journeyPatternId: Option[NonEmptyString],
    timeFrame: LocalDate,
    vehicleJourneyId: NonEmptyString,
    operator: NonEmptyString,
    congestion: Boolean,
    lon: BigDecimal,
    lat: BigDecimal,
    delay: Option[Int],
    blockId: NonEmptyString,
    vehicleId: NonEmptyString,
    stopId: NonEmptyString,
    atStop: Boolean
)
