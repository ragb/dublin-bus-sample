package com.ruiandrebatista.bus

import java.nio.file.Path
import kantan.csv._
import kantan.csv.generic._
import kantan.csv.ops._
import kantan.csv.refined._
import kantan.csv.java8._

import cats.implicits._
import cats.effect._
import fs2._

import com.ruiandrebatista.bus.model.RawCsvRow

import java.net.URL

import ciris._

import ciris.cats.effect._

import com.ruiandrebatista.bus.data.PostgresBusLoadRepository

import com.typesafe.scalalogging.LazyLogging

object CsvLoader extends AbstractApp with LazyLogging {

  def csvConfig(args: IndexedSeq[String]) =
    loadConfig(
      argF[IO, Path](args)(0)
    ) { identity _ }.orRaiseThrowable

  // Decode 0-1 values as booleans
  implicit val booleanDecoder: CellDecoder[Boolean] =
    CellDecoder[Int].map(_ != 0)

  def runFromConfig(args: List[String], config: Config) =
    csvConfig(args.toIndexedSeq).flatMap { csvFile =>
      resources(config).use {
        case AppResources(_, transactor) =>
          val repo = new PostgresBusLoadRepository(transactor)

          val data = readRaw(csvFile.toUri.toURL)

          repo
            .loadData(data)
            .flatMap { result =>
              IO(logger.info(s"Loaded ${result.rowsLoaded} total rows")) *>
                ExitCode(0).pure[IO]
            }
      }
    }

  private[bus] def readRaw(resource: URL) = {
    Stream
      .bracket(IO(resource.asCsvReader[RawCsvRow](rfc.withoutHeader)))(
        r => IO(r.close)
      )
      .flatMap { reader =>
        Stream
          .fromIterator[IO, ReadResult[RawCsvRow]](reader.toIterator)
          .zipWithIndex
          .flatMap {
            case (element, index) =>
              // have the line number on errors for easier debugging
              Stream.fromEither[IO](
                element.left
                  .map(e => new Exception(s"Line $index, ${e.getMessage}", e))
              )

          }

      }

  }

}
