CREATE TABLE "bus_data"(
       "timestamp" TIMESTAMP,
       "operator" CHAR(2),
       "vehicle_id" TEXT,
       "coordinates" POINT,
       "stop_id" TEXT,
       "at_stop" BOOL,
	   UNIQUE("timestamp", "vehicle_id")
);

CREATE INDEX "ts_op_ve_idx" on
       "bus_data"("timestamp", "operator", "vehicle_id");
