package com.ruiandrebatista.bus

import org.specs2.mutable.Specification
import org.specs2.matcher.Matchers

import cats.effect._
import scala.concurrent.ExecutionContext.Implicits.global

class CsvLoaderTest extends Specification with Matchers {

  private implicit val cs = IO.contextShift(global)

  "CsvLoader" should {
    "Load sample entries" in {

      val sampleData = getClass().getResource("/sample.csv")

      CSVLoader
        .readRaw(sampleData)
        .compile
        .toVector
        .unsafeRunSync should have size 100

    }
  }
}
