package com.ruiandrebatista.bus

import cats.effect._
import cats.implicits._
import org.specs2.mutable.Specification
import org.specs2.matcher.Matchers

import org.http4s._
import org.http4s.implicits._
import org.http4s.circe._
import io.circe._
import io.circe.syntax._
import io.circe.refined._
import com.ruiandrebatista.bus.data.BusQueryRepository
import eu.timepit.refined.types.string.NonEmptyString
import java.time.Instant
import com.ruiandrebatista.bus.data.TraceEntry

import fs2._

import eu.timepit.refined.auto._

import scala.concurrent.ExecutionContext.Implicits.global

class HttpApiTest extends Specification with Matchers {

  implicit val cs = IO.contextShift(global)

  val runningOperators = Vector[NonEmptyString]("D1", "D2", "D3")
  val runningVehicleIds = Vector[NonEmptyString]("1", "2")
  val trace = Stream.emit[IO, TraceEntry](
    TraceEntry(Instant.parse("2019-01-01T03:00:05Z"), (6.2, 53.3), None))

  val dummyQueryRepository = new BusQueryRepository[IO] {
    def listRunningOperators(start: Instant,
                             end: Instant): IO[Vector[NonEmptyString]] =
      runningOperators.pure[IO]
    def listRunningVehicleIds(operator: NonEmptyString,
                              start: Instant,
                              end: Instant): IO[Vector[NonEmptyString]] =
      runningVehicleIds.pure[IO]
    def listRunningVehicleIdsAtStop(operator: NonEmptyString,
                                    start: Instant,
                                    end: Instant): IO[Vector[NonEmptyString]] =
      runningVehicleIds.pure[IO]
    def traceVehicle(vehicleId: NonEmptyString,
                     start: Instant,
                     end: Instant): fs2.Stream[IO, TraceEntry] = trace
  }

  val startTime = "2019-01-01T04:09:44"
  val endTime = "2019-01-01T04:10:53"

  val api = new HttpApi(dummyQueryRepository)

  "HttpApi" should {
    "Responde to running operators" in {

      val request =
        Request[IO](Method.GET,
                    Uri.unsafeFromString(
                      s"operators?start-time=$startTime&end-time=$endTime"))

      val response = api.routes.orNotFound.run(request).unsafeRunSync()

      response.as[Json].unsafeRunSync() should beEqualTo(
        runningOperators.asJson)

    }

    "Responde to running vehicles" in {

      val request = Request[IO](
        Method.GET,
        Uri.unsafeFromString(
          s"operators/D1/vehicles?start-time=$startTime&end-time=$endTime"))

      val response = api.routes.orNotFound.run(request).unsafeRunSync()
      response.status.code === 200
      response.as[Json].unsafeRunSync() should beEqualTo(
        runningVehicleIds.asJson)
    }

    "Responde to running vehicles at stop" in {

      val request = Request[IO](
        Method.GET,
        Uri.unsafeFromString(
          s"operators/D1/stoped-vehicles?start-time=$startTime&end-time=$endTime"))

      val response = api.routes.orNotFound.run(request).unsafeRunSync()
      response.status.code === 200
      response.as[Json].unsafeRunSync() should beEqualTo(
        runningVehicleIds.asJson)
    }

    "Respond to trace vehicle" in {

      val request = Request[IO](
        Method.GET,
        Uri.unsafeFromString(
          s"vehicles/1/trace?start-time=$startTime&end-time=$endTime"))

      val response = api.routes.orNotFound.run(request).unsafeRunSync()
      response.status.code === 200
      response.as[Json].unsafeRunSync() should beEqualTo(
        trace.compile.to[List].unsafeRunSync().asJson)
    }

    // TODO check for error conditions etc.
  }
}
